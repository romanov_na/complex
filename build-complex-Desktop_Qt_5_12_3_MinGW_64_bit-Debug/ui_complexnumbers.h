/********************************************************************************
** Form generated from reading UI file 'complexnumbers.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COMPLEXNUMBERS_H
#define UI_COMPLEXNUMBERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ComplexNumbers
{
public:
    QWidget *centralWidget;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *img;
    QLabel *real;
    QLabel *label_4;
    QLabel *label_2;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QPushButton *calc51;
    QPushButton *calc91;
    QPushButton *calc21;
    QPushButton *calc31;
    QPushButton *calc41;
    QPushButton *calc81;
    QPushButton *calc61;
    QPushButton *calc11;
    QPushButton *calc01;
    QPushButton *calc71;
    QPushButton *calcDot1;
    QPushButton *calcSign1;
    QWidget *gridLayoutWidget_3;
    QGridLayout *gridLayout_3;
    QPushButton *calc52;
    QPushButton *calc92;
    QPushButton *calc22;
    QPushButton *calc32;
    QPushButton *calc42;
    QPushButton *calc82;
    QPushButton *calc62;
    QPushButton *calc12;
    QPushButton *calc02;
    QPushButton *calc72;
    QPushButton *calcDot2;
    QPushButton *calcSign2;
    QWidget *horizontalLayoutWidget;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pushButton_2;
    QPushButton *sqr;
    QPushButton *deg;
    QPushButton *div;
    QPushButton *mult;
    QPushButton *minus;
    QPushButton *sum;
    QPushButton *pushButton;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_3;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *label;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *ComplexNumbers)
    {
        if (ComplexNumbers->objectName().isEmpty())
            ComplexNumbers->setObjectName(QString::fromUtf8("ComplexNumbers"));
        ComplexNumbers->resize(897, 698);
        centralWidget = new QWidget(ComplexNumbers);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayoutWidget = new QWidget(centralWidget);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 0, 471, 81));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        img = new QLabel(gridLayoutWidget);
        img->setObjectName(QString::fromUtf8("img"));
        QFont font;
        font.setPointSize(18);
        img->setFont(font);
        img->setStyleSheet(QString::fromUtf8("QLabel\n"
"{\n"
"qproperty-alignment:'AlignVCenter| AlignRight';\n"
"border: 1px solid gray;\n"
"}"));

        gridLayout->addWidget(img, 0, 3, 1, 1);

        real = new QLabel(gridLayoutWidget);
        real->setObjectName(QString::fromUtf8("real"));
        real->setFont(font);
        real->setStyleSheet(QString::fromUtf8("QLabel\n"
"{\n"
"qproperty-alignment:'AlignVCenter| AlignRight';\n"
"border: 1px solid gray;\n"
"}"));

        gridLayout->addWidget(real, 0, 0, 1, 1);

        label_4 = new QLabel(gridLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QFont font1;
        font1.setPointSize(20);
        label_4->setFont(font1);

        gridLayout->addWidget(label_4, 0, 4, 1, 1);

        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font2;
        font2.setPointSize(24);
        label_2->setFont(font2);

        gridLayout->addWidget(label_2, 0, 1, 1, 1);

        gridLayoutWidget_2 = new QWidget(centralWidget);
        gridLayoutWidget_2->setObjectName(QString::fromUtf8("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(0, 80, 239, 231));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(gridLayoutWidget_2->sizePolicy().hasHeightForWidth());
        gridLayoutWidget_2->setSizePolicy(sizePolicy);
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        calc51 = new QPushButton(gridLayoutWidget_2);
        calc51->setObjectName(QString::fromUtf8("calc51"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(calc51->sizePolicy().hasHeightForWidth());
        calc51->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(calc51, 1, 1, 1, 1);

        calc91 = new QPushButton(gridLayoutWidget_2);
        calc91->setObjectName(QString::fromUtf8("calc91"));
        sizePolicy1.setHeightForWidth(calc91->sizePolicy().hasHeightForWidth());
        calc91->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(calc91, 0, 2, 1, 1);

        calc21 = new QPushButton(gridLayoutWidget_2);
        calc21->setObjectName(QString::fromUtf8("calc21"));
        sizePolicy1.setHeightForWidth(calc21->sizePolicy().hasHeightForWidth());
        calc21->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(calc21, 2, 1, 1, 1);

        calc31 = new QPushButton(gridLayoutWidget_2);
        calc31->setObjectName(QString::fromUtf8("calc31"));
        sizePolicy1.setHeightForWidth(calc31->sizePolicy().hasHeightForWidth());
        calc31->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(calc31, 2, 2, 1, 1);

        calc41 = new QPushButton(gridLayoutWidget_2);
        calc41->setObjectName(QString::fromUtf8("calc41"));
        sizePolicy1.setHeightForWidth(calc41->sizePolicy().hasHeightForWidth());
        calc41->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(calc41, 1, 0, 1, 1);

        calc81 = new QPushButton(gridLayoutWidget_2);
        calc81->setObjectName(QString::fromUtf8("calc81"));
        sizePolicy1.setHeightForWidth(calc81->sizePolicy().hasHeightForWidth());
        calc81->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(calc81, 0, 1, 1, 1);

        calc61 = new QPushButton(gridLayoutWidget_2);
        calc61->setObjectName(QString::fromUtf8("calc61"));
        sizePolicy1.setHeightForWidth(calc61->sizePolicy().hasHeightForWidth());
        calc61->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(calc61, 1, 2, 1, 1);

        calc11 = new QPushButton(gridLayoutWidget_2);
        calc11->setObjectName(QString::fromUtf8("calc11"));
        sizePolicy1.setHeightForWidth(calc11->sizePolicy().hasHeightForWidth());
        calc11->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(calc11, 2, 0, 1, 1);

        calc01 = new QPushButton(gridLayoutWidget_2);
        calc01->setObjectName(QString::fromUtf8("calc01"));
        sizePolicy1.setHeightForWidth(calc01->sizePolicy().hasHeightForWidth());
        calc01->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(calc01, 3, 1, 1, 1);

        calc71 = new QPushButton(gridLayoutWidget_2);
        calc71->setObjectName(QString::fromUtf8("calc71"));
        calc71->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Ignored, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(calc71->sizePolicy().hasHeightForWidth());
        calc71->setSizePolicy(sizePolicy2);
        calc71->setLayoutDirection(Qt::LeftToRight);

        gridLayout_2->addWidget(calc71, 0, 0, 1, 1);

        calcDot1 = new QPushButton(gridLayoutWidget_2);
        calcDot1->setObjectName(QString::fromUtf8("calcDot1"));
        sizePolicy1.setHeightForWidth(calcDot1->sizePolicy().hasHeightForWidth());
        calcDot1->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(calcDot1, 3, 0, 1, 1);

        calcSign1 = new QPushButton(gridLayoutWidget_2);
        calcSign1->setObjectName(QString::fromUtf8("calcSign1"));
        sizePolicy1.setHeightForWidth(calcSign1->sizePolicy().hasHeightForWidth());
        calcSign1->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(calcSign1, 3, 2, 1, 1);

        gridLayoutWidget_3 = new QWidget(centralWidget);
        gridLayoutWidget_3->setObjectName(QString::fromUtf8("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(240, 80, 241, 231));
        sizePolicy.setHeightForWidth(gridLayoutWidget_3->sizePolicy().hasHeightForWidth());
        gridLayoutWidget_3->setSizePolicy(sizePolicy);
        gridLayout_3 = new QGridLayout(gridLayoutWidget_3);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        calc52 = new QPushButton(gridLayoutWidget_3);
        calc52->setObjectName(QString::fromUtf8("calc52"));
        sizePolicy1.setHeightForWidth(calc52->sizePolicy().hasHeightForWidth());
        calc52->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(calc52, 1, 1, 1, 1);

        calc92 = new QPushButton(gridLayoutWidget_3);
        calc92->setObjectName(QString::fromUtf8("calc92"));
        sizePolicy1.setHeightForWidth(calc92->sizePolicy().hasHeightForWidth());
        calc92->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(calc92, 0, 2, 1, 1);

        calc22 = new QPushButton(gridLayoutWidget_3);
        calc22->setObjectName(QString::fromUtf8("calc22"));
        sizePolicy1.setHeightForWidth(calc22->sizePolicy().hasHeightForWidth());
        calc22->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(calc22, 2, 1, 1, 1);

        calc32 = new QPushButton(gridLayoutWidget_3);
        calc32->setObjectName(QString::fromUtf8("calc32"));
        sizePolicy1.setHeightForWidth(calc32->sizePolicy().hasHeightForWidth());
        calc32->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(calc32, 2, 2, 1, 1);

        calc42 = new QPushButton(gridLayoutWidget_3);
        calc42->setObjectName(QString::fromUtf8("calc42"));
        sizePolicy1.setHeightForWidth(calc42->sizePolicy().hasHeightForWidth());
        calc42->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(calc42, 1, 0, 1, 1);

        calc82 = new QPushButton(gridLayoutWidget_3);
        calc82->setObjectName(QString::fromUtf8("calc82"));
        sizePolicy1.setHeightForWidth(calc82->sizePolicy().hasHeightForWidth());
        calc82->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(calc82, 0, 1, 1, 1);

        calc62 = new QPushButton(gridLayoutWidget_3);
        calc62->setObjectName(QString::fromUtf8("calc62"));
        sizePolicy1.setHeightForWidth(calc62->sizePolicy().hasHeightForWidth());
        calc62->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(calc62, 1, 2, 1, 1);

        calc12 = new QPushButton(gridLayoutWidget_3);
        calc12->setObjectName(QString::fromUtf8("calc12"));
        sizePolicy1.setHeightForWidth(calc12->sizePolicy().hasHeightForWidth());
        calc12->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(calc12, 2, 0, 1, 1);

        calc02 = new QPushButton(gridLayoutWidget_3);
        calc02->setObjectName(QString::fromUtf8("calc02"));
        sizePolicy1.setHeightForWidth(calc02->sizePolicy().hasHeightForWidth());
        calc02->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(calc02, 3, 1, 1, 1);

        calc72 = new QPushButton(gridLayoutWidget_3);
        calc72->setObjectName(QString::fromUtf8("calc72"));
        calc72->setEnabled(true);
        sizePolicy2.setHeightForWidth(calc72->sizePolicy().hasHeightForWidth());
        calc72->setSizePolicy(sizePolicy2);
        calc72->setLayoutDirection(Qt::LeftToRight);

        gridLayout_3->addWidget(calc72, 0, 0, 1, 1);

        calcDot2 = new QPushButton(gridLayoutWidget_3);
        calcDot2->setObjectName(QString::fromUtf8("calcDot2"));
        sizePolicy1.setHeightForWidth(calcDot2->sizePolicy().hasHeightForWidth());
        calcDot2->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(calcDot2, 3, 0, 1, 1);

        calcSign2 = new QPushButton(gridLayoutWidget_3);
        calcSign2->setObjectName(QString::fromUtf8("calcSign2"));
        sizePolicy1.setHeightForWidth(calcSign2->sizePolicy().hasHeightForWidth());
        calcSign2->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(calcSign2, 3, 2, 1, 1);

        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(0, 310, 481, 356));
        verticalLayout_2 = new QVBoxLayout(horizontalLayoutWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        pushButton_2 = new QPushButton(horizontalLayoutWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        QFont font3;
        font3.setPointSize(16);
        pushButton_2->setFont(font3);

        verticalLayout_2->addWidget(pushButton_2);

        sqr = new QPushButton(horizontalLayoutWidget);
        sqr->setObjectName(QString::fromUtf8("sqr"));
        sizePolicy1.setHeightForWidth(sqr->sizePolicy().hasHeightForWidth());
        sqr->setSizePolicy(sizePolicy1);
        sqr->setFont(font1);

        verticalLayout_2->addWidget(sqr);

        deg = new QPushButton(horizontalLayoutWidget);
        deg->setObjectName(QString::fromUtf8("deg"));
        sizePolicy1.setHeightForWidth(deg->sizePolicy().hasHeightForWidth());
        deg->setSizePolicy(sizePolicy1);
        deg->setFont(font1);

        verticalLayout_2->addWidget(deg);

        div = new QPushButton(horizontalLayoutWidget);
        div->setObjectName(QString::fromUtf8("div"));
        sizePolicy1.setHeightForWidth(div->sizePolicy().hasHeightForWidth());
        div->setSizePolicy(sizePolicy1);
        div->setFont(font1);

        verticalLayout_2->addWidget(div);

        mult = new QPushButton(horizontalLayoutWidget);
        mult->setObjectName(QString::fromUtf8("mult"));
        sizePolicy1.setHeightForWidth(mult->sizePolicy().hasHeightForWidth());
        mult->setSizePolicy(sizePolicy1);
        mult->setFont(font1);

        verticalLayout_2->addWidget(mult);

        minus = new QPushButton(horizontalLayoutWidget);
        minus->setObjectName(QString::fromUtf8("minus"));
        sizePolicy1.setHeightForWidth(minus->sizePolicy().hasHeightForWidth());
        minus->setSizePolicy(sizePolicy1);
        minus->setFont(font1);

        verticalLayout_2->addWidget(minus);

        sum = new QPushButton(horizontalLayoutWidget);
        sum->setObjectName(QString::fromUtf8("sum"));
        sizePolicy1.setHeightForWidth(sum->sizePolicy().hasHeightForWidth());
        sum->setSizePolicy(sizePolicy1);
        sum->setFont(font1);

        verticalLayout_2->addWidget(sum);

        pushButton = new QPushButton(horizontalLayoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font3);

        verticalLayout_2->addWidget(pushButton);

        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(479, 79, 411, 581));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);
        label_3->setStyleSheet(QString::fromUtf8("QLabel\n"
"{\n"
"qproperty-alignment:'AlignVCenter| AlignCenter';\n"
"border: 1px solid gray;\n"
"}"));

        verticalLayout->addWidget(label_3);

        verticalLayoutWidget_2 = new QWidget(centralWidget);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(479, 0, 411, 80));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(verticalLayoutWidget_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font3);
        label->setStyleSheet(QString::fromUtf8("QLabel\n"
"{\n"
"qproperty-alignment:'AlignVCenter| AlignCenter';\n"
"border: 1px solid gray;\n"
"}"));

        verticalLayout_3->addWidget(label);

        ComplexNumbers->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(ComplexNumbers);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 897, 21));
        ComplexNumbers->setMenuBar(menuBar);
        mainToolBar = new QToolBar(ComplexNumbers);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        ComplexNumbers->addToolBar(Qt::TopToolBarArea, mainToolBar);

        retranslateUi(ComplexNumbers);
        QObject::connect(sqr, SIGNAL(clicked()), label_2, SLOT(hide()));
        QObject::connect(sqr, SIGNAL(clicked()), img, SLOT(hide()));
        QObject::connect(sqr, SIGNAL(clicked()), label_4, SLOT(hide()));
        QObject::connect(sqr, SIGNAL(clicked()), deg, SLOT(hide()));
        QObject::connect(sqr, SIGNAL(clicked()), div, SLOT(hide()));
        QObject::connect(sqr, SIGNAL(clicked()), mult, SLOT(hide()));
        QObject::connect(sqr, SIGNAL(clicked()), minus, SLOT(hide()));
        QObject::connect(sqr, SIGNAL(clicked()), sum, SLOT(hide()));
        QObject::connect(deg, SIGNAL(clicked()), label_2, SLOT(hide()));
        QObject::connect(deg, SIGNAL(clicked()), img, SLOT(hide()));
        QObject::connect(deg, SIGNAL(clicked()), label_4, SLOT(hide()));
        QObject::connect(deg, SIGNAL(clicked()), div, SLOT(hide()));
        QObject::connect(deg, SIGNAL(clicked()), sqr, SLOT(hide()));
        QObject::connect(deg, SIGNAL(clicked()), mult, SLOT(hide()));
        QObject::connect(deg, SIGNAL(clicked()), minus, SLOT(hide()));
        QObject::connect(deg, SIGNAL(clicked()), sum, SLOT(hide()));
        QObject::connect(div, SIGNAL(clicked()), sqr, SLOT(hide()));
        QObject::connect(div, SIGNAL(clicked()), deg, SLOT(hide()));
        QObject::connect(div, SIGNAL(clicked()), mult, SLOT(hide()));
        QObject::connect(div, SIGNAL(clicked()), minus, SLOT(hide()));
        QObject::connect(div, SIGNAL(clicked()), sum, SLOT(hide()));
        QObject::connect(mult, SIGNAL(clicked()), sqr, SLOT(hide()));
        QObject::connect(mult, SIGNAL(clicked()), deg, SLOT(hide()));
        QObject::connect(mult, SIGNAL(clicked()), div, SLOT(hide()));
        QObject::connect(mult, SIGNAL(clicked()), minus, SLOT(hide()));
        QObject::connect(mult, SIGNAL(clicked()), sum, SLOT(hide()));
        QObject::connect(minus, SIGNAL(clicked()), sqr, SLOT(hide()));
        QObject::connect(minus, SIGNAL(clicked()), deg, SLOT(hide()));
        QObject::connect(minus, SIGNAL(clicked()), div, SLOT(hide()));
        QObject::connect(minus, SIGNAL(clicked()), mult, SLOT(hide()));
        QObject::connect(minus, SIGNAL(clicked()), sum, SLOT(hide()));
        QObject::connect(sum, SIGNAL(clicked()), sqr, SLOT(hide()));
        QObject::connect(sum, SIGNAL(clicked()), deg, SLOT(hide()));
        QObject::connect(sum, SIGNAL(clicked()), div, SLOT(hide()));
        QObject::connect(sum, SIGNAL(clicked()), mult, SLOT(hide()));
        QObject::connect(sum, SIGNAL(clicked()), minus, SLOT(hide()));
        QObject::connect(pushButton, SIGNAL(clicked()), sum, SLOT(show()));
        QObject::connect(pushButton, SIGNAL(clicked()), minus, SLOT(show()));
        QObject::connect(pushButton, SIGNAL(clicked()), mult, SLOT(show()));
        QObject::connect(pushButton, SIGNAL(clicked()), div, SLOT(show()));
        QObject::connect(pushButton, SIGNAL(clicked()), deg, SLOT(show()));
        QObject::connect(pushButton, SIGNAL(clicked()), sqr, SLOT(show()));
        QObject::connect(pushButton, SIGNAL(clicked()), label_2, SLOT(show()));
        QObject::connect(pushButton, SIGNAL(clicked()), img, SLOT(show()));
        QObject::connect(pushButton, SIGNAL(clicked()), label_4, SLOT(show()));

        QMetaObject::connectSlotsByName(ComplexNumbers);
    } // setupUi

    void retranslateUi(QMainWindow *ComplexNumbers)
    {
        ComplexNumbers->setWindowTitle(QApplication::translate("ComplexNumbers", "ComplexNumbers", nullptr));
        img->setText(QApplication::translate("ComplexNumbers", "0", nullptr));
        real->setText(QApplication::translate("ComplexNumbers", "0", nullptr));
        label_4->setText(QApplication::translate("ComplexNumbers", "i", nullptr));
        label_2->setText(QApplication::translate("ComplexNumbers", ";", nullptr));
        calc51->setText(QApplication::translate("ComplexNumbers", "5", nullptr));
        calc91->setText(QApplication::translate("ComplexNumbers", "9", nullptr));
        calc21->setText(QApplication::translate("ComplexNumbers", "2", nullptr));
        calc31->setText(QApplication::translate("ComplexNumbers", "3", nullptr));
        calc41->setText(QApplication::translate("ComplexNumbers", "4", nullptr));
        calc81->setText(QApplication::translate("ComplexNumbers", "8", nullptr));
        calc61->setText(QApplication::translate("ComplexNumbers", "6", nullptr));
        calc11->setText(QApplication::translate("ComplexNumbers", "1", nullptr));
        calc01->setText(QApplication::translate("ComplexNumbers", "0", nullptr));
        calc71->setText(QApplication::translate("ComplexNumbers", "7", nullptr));
        calcDot1->setText(QApplication::translate("ComplexNumbers", ".", nullptr));
        calcSign1->setText(QApplication::translate("ComplexNumbers", "+/-", nullptr));
        calc52->setText(QApplication::translate("ComplexNumbers", "5", nullptr));
        calc92->setText(QApplication::translate("ComplexNumbers", "9", nullptr));
        calc22->setText(QApplication::translate("ComplexNumbers", "2", nullptr));
        calc32->setText(QApplication::translate("ComplexNumbers", "3", nullptr));
        calc42->setText(QApplication::translate("ComplexNumbers", "4", nullptr));
        calc82->setText(QApplication::translate("ComplexNumbers", "8", nullptr));
        calc62->setText(QApplication::translate("ComplexNumbers", "6", nullptr));
        calc12->setText(QApplication::translate("ComplexNumbers", "1", nullptr));
        calc02->setText(QApplication::translate("ComplexNumbers", "0", nullptr));
        calc72->setText(QApplication::translate("ComplexNumbers", "7", nullptr));
        calcDot2->setText(QApplication::translate("ComplexNumbers", ".", nullptr));
        calcSign2->setText(QApplication::translate("ComplexNumbers", "+/-", nullptr));
        pushButton_2->setText(QApplication::translate("ComplexNumbers", "AC", nullptr));
        sqr->setText(QApplication::translate("ComplexNumbers", "v", nullptr));
        deg->setText(QApplication::translate("ComplexNumbers", "^", nullptr));
        div->setText(QApplication::translate("ComplexNumbers", "/", nullptr));
        mult->setText(QApplication::translate("ComplexNumbers", "x", nullptr));
        minus->setText(QApplication::translate("ComplexNumbers", "-", nullptr));
        sum->setText(QApplication::translate("ComplexNumbers", "+", nullptr));
        pushButton->setText(QApplication::translate("ComplexNumbers", "=", nullptr));
        label_3->setText(QApplication::translate("ComplexNumbers", "0", nullptr));
        label->setText(QApplication::translate("ComplexNumbers", "\320\236\321\202\320\262\320\265\321\202", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ComplexNumbers: public Ui_ComplexNumbers {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COMPLEXNUMBERS_H
