@echo off
SetLocal EnableDelayedExpansion
(set PATH=E:\newQt\5.12.3\mingw73_64\bin;!PATH!)
if defined QT_PLUGIN_PATH (
    set QT_PLUGIN_PATH=E:\newQt\5.12.3\mingw73_64\plugins;!QT_PLUGIN_PATH!
) else (
    set QT_PLUGIN_PATH=E:\newQt\5.12.3\mingw73_64\plugins
)
%*
EndLocal
