#ifndef COMPLEXNUMBERS_H
#define COMPLEXNUMBERS_H

#include <QMainWindow>


namespace Ui {
class ComplexNumbers;
}

class ComplexNumbers : public QMainWindow
{
    Q_OBJECT

public:
    explicit ComplexNumbers(QWidget *parent = nullptr);
    ~ComplexNumbers();

private slots:
    void on_pushButton_clicked();

private:
    Ui::ComplexNumbers *ui;
private slots:
    void digit_numbers();
    void digit_img();
    void on_calcDot1_clicked();
    void on_calcDot2_clicked();
    void sign1();
    void sign2();
    void on_pushButton_2_clicked();
    void math_operations();
    void pushAC();
};

#endif // COMPLEXNUMBERS_H


