#include "functions.h"
#include <qmath.h>

QString deg (double a, double b, int n)
{
    double real = 0;
    double img = 0;
    double sq = sqrt(qPow(a,2)+qPow(b,2));
    QString s = "";
    if (a>0)
    {
         real = (qPow(sq,n) * cos(n*atan(b/a)));
         img = (qPow(sq,n) * sin(n*atan(b/a)));
    }
    if (a<0)
    {
         real = (qPow(sq,n) * cos(n*(atan(b/a)+M_PI)));
         img = (qPow(sq,n) * sin(n*atan(b/a)+M_PI));
    }

    s = QString("%1;%2i")
            .arg(real).arg(img);
    return s;
}
