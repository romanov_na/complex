#include "functions.h"
#include <QtMath>

QString div (double a, double b, double c, double d)
{
    QString s = "";
    if (c != 0.0 || d != 0.0)
    {
    double m = qPow(c,2) + qPow(d,2);
    double real = ((a * c + b * d)/m);
    double img = ((b*c - a*d)/m);
    s = QString("%1;%2i")
            .arg(real).arg(img);
    }else s = "error";
    return s;
}
