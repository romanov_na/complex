#include "functions.h"

QString mult (double a, double b, double c, double d)
{
    QString s = "";
    double real = (a * b - c * d);
    double img = (a*d + b*c);
    s = QString("%1;%2i")
            .arg(real).arg(img);
    return s;
}
