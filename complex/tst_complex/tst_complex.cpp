#include <QtTest>
#include <../functions.h>
#include "tst_complex.h"


// add necessary includes here

Test_Complex::Test_Complex()
{

}

void Test_Complex::test_sum()
{
    double a = 10; double b = 10; double c = 5; double d = 5;
        QCOMPARE("15;15i", sum(a, b, c, d));

}

void Test_Complex::test_minus()
{
    double a = 10; double b = 10; double c = 5; double d = 5;
        QCOMPARE("5;5i", minus(a, b, c, d));

}

void Test_Complex::test_mult()
{
    double a = 10; double b = 10; double c = 5; double d = 5;
        QCOMPARE("75;100i", mult(a, b, c, d));

}

void Test_Complex::test_div()




{
    double a = 10; double b = 10; double c = 5; double d = 5;
        QCOMPARE("2;0i", div(a, b, c, d));

}

void Test_Complex::test_deg()
{
    double a = 1; double b = 2; int n = 2;
        QCOMPARE("-3;4i", deg(a, b, n));

}



void Test_Complex::test_sqrt()


{
    double a = 1; double b = 2; int n = 2;
        QCOMPARE("4.25325;2.62866i", sqrt(a, b, n));

}



//QTEST_APPLESS_MAIN(Test_Mass)

//#include "tst_test_mass.moc"
